import { InertiaLink } from '@inertiajs/inertia-react'
import React from 'react'

const NavBar = ({ navItem }) => {
  return (

    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <InertiaLink className="navbar-brand" href={`${base_url}/`}>Blog</InertiaLink>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <InertiaLink className="nav-link" href={`${base_url}/`}>Home <span className="sr-only">(current)</span></InertiaLink>
            </li>
            <li className="nav-item">
              {/* <InertiaLink className="nav-link" href={`${base_url}/users`}>User</InertiaLink> */}
              <InertiaLink className="nav-link" href={route('users.index')}>User</InertiaLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>

  )
}

export default NavBar
