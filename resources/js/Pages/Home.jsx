import React from "react"
import Front from "../Layouts/Front"


const Home = () => {
  return (
    <Front pageTitle="Home page">
      <div className="container">
        <h2>Laravel inertia boilerplate abbas</h2>
      </div>
    </Front>

  )
}

export default Home