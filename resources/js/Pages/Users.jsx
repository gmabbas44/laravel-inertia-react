import { InertiaLink } from "@inertiajs/inertia-react"
import React from "react"
import Front from "../Layouts/Front"

const Users = ({ users, create_url }) => {
  // console.log(users, create_url)
  return (
    <Front pageTitle="All user">
      <h2 className="text-center">Show user information</h2>
      <table className="table table-sm">
        <thead className="thead">
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Edit</th>
            <th>delete</th>
          </tr>
        </thead>
        <tbody>
          {
            users.map(user => (
              <tr key={`user-id${user.id}`}>
                <td>{user.id}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td><InertiaLink href={user.edit_url}>Edit</InertiaLink></td>
                <td><InertiaLink href={user.delete_url}>Delete</InertiaLink></td>
              </tr>
            ))
          }

        </tbody>
      </table>
    </Front>
  )
}

export default Users