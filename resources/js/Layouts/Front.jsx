import React, { useEffect } from 'react'
import NavBar from '../components/NavBar'

const Front = ({ pageTitle, children }) => {
  useEffect(() => {
    document.title = pageTitle
  }, [])
  return (
    <>
      <NavBar />
      <main className="container">
        {children}
      </main>
    </>
  )
}

export default Front
